#!/bin/bash

ENV="$1";
PRJ="$2";
VM="$3";
DIR="data/www/htdocs";
CUR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd );
COP="docker-compose.yml";
source ~/.bash_profile;

runDockerComposeSetup()
{
	if [ "$ENV" = "" ]
	then
		echo "Please provide the environment: local, dev, stage, prod, jenkins";
		exit 1;
	fi

	if [[ "$PRJ" = "" && "$ENV" != "local" ]]
	then
		echo "Please provide the environment project name: (ex: MyProjDev)";
		exit 1;
	fi

	if [[ "$ENV" != "" && "$PRJ" != "" && "$VM" = "vm" ]]
	then
		docker-compose stop;
		copyDockerComposeFile;

		CHECK="$(docker-machine ip $PRJ)";
		sleep 3;

		if [[ $CHECK == *"unable to load host"* || $CHECK == *"machine does not exist"* ]]
		then
			docker-machine create -d virtualbox "$PRJ";
			sleep 5;
		fi

		if [[ $CHECK == *"host is not running"* ]]
		then
			docker-machine start "$PRJ";
			sleep 5;
		fi

		EXPORTS="$(docker-machine env $PRJ)";
		sleep 2;
		eval $EXPORTS;

		sleep 2;
		docker-compose up -d;
		docker-compose ps;
	else
		stopServices;
		copyFiles;
		copyDockerComposeFile;
		startServices;
		listServices;
	fi
}

listServices()
{
	if [ "$PRJ" != "" ]
	then
		docker-compose -p "$PRJ" ps;
	else
		docker-compose ps;
	fi
}

startServices()
{
	if [ "$PRJ" != "" ]
	then
		docker-compose -p "$PRJ" up -d;
	else
		docker-compose up -d;
	fi
}

stopServices()
{
	if [ -e "$COP" ]
	then
		if [ "$PRJ" != "" ]
		then
			docker-compose -p "$PRJ" stop;
		else
			docker-compose stop;
		fi
	fi
}

copyDockerComposeFile()
{
	cp "env/$ENV/docker-compose.yml" "$COP";
}

copyFiles()
{
	#cp "env/$1/env.config" "$DIR/.env";
	echo "No files to copy over";
}

runComposer()
{
	if [ "$PRJ" != "" ]
	then
		docker-compose -p "$PRJ" run composer self-update;
		docker-compose -p "$PRJ" run composer update;
	else
		docker-compose run composer self-update;
		docker-compose run composer update;
	fi
}

createFolders()
{
	mkdir -p data/logs;
}

createFolders;
runDockerComposeSetup;

if [ -d "$DIR" ]
then
	copyFiles;
else
	runComposer;
	copyFiles;
	startServices;
	listServices;
fi
