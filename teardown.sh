#!/bin/bash

ENV="$1";
PRJ="$2";
VM="$3";
DIR="data/www/htdocs";
source ~/.bash_profile;

runDockerComposeTeardown()
{
	if [ "$ENV" = "" ]
	then
		echo "Please provide the environment: local, dev, stage, prod, jenkins";
		exit 1;
	fi

	if [[ "$PRJ" = "" && "$ENV" != "local" ]]
	then
		echo "Please provide the environment project name: (ex: MyProjDev)";
		exit 1;
	fi

	if [[ "$ENV" != "" && "$PRJ" != "" && "$VM" = "vm" ]]
	then
		EXPORTS="$(docker-machine env $PRJ)";
		sleep 2;
		eval $EXPORTS;
		sleep 2;
		
		docker-compose stop;
		docker-machine stop "$PRJ";
	else
		stopServices;
	fi
}

stopServices()
{
	if [ "$PRJ" != "" ]
	then
		docker-compose -p "$PRJ" stop;
	else
		docker-compose stop;
	fi
}

removeDockerComposeFile()
{
	rm -f "docker-compose.yml";
}

removeFiles()
{
	#rm -f "$DIR/.env";
	echo "No files to remove";
}

runDockerComposeTeardown;
removeDockerComposeFile;

if [ -d "$DIR" ]
then
	removeFiles;
fi

